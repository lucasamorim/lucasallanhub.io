---
layout: post
title:  "New year. New blog!"
date:   2014-01-04 23:11:39
---

5 years ago when I started my blog I didn't really know about what I would be writing.
I started writing a lot about Linux, Java and Ruby. But sometimes I wrote posts that was not related
to this subjects.

### Anyway, what is the reason to create a new blog?

The answer is: no reason. I was tired of my old blog, mostly of the content there are outdated and I didn't
see any reason to waste time migrating outdated data to this new blog.

In this new blog, I want to write only about tech stuff. Like the programming languages and frameworks I'm using.

### OpenSource Projects

  Right now, I'm involved with a lot of cool stuff. I've been doing some contributions for [JRuby][jruby]
(my goal is send at least two pull requests each month).

I'm also working on the [cramp][cramp] (Real-time web application framework in Ruby) - this is an awesome project
that unfortenelly didn't receive too much love in the last year.

### Two new programming languanges

* **Scala**
It's been awhile that I'm interested in [Functional Programming][fp] and one month ago I finished my first project using Scala.

* **Elixir**
A few months ago I bought the [Elixir book][elixir-book] - although I didn't finish yet.
I have to say I'm really in love with Elixir.
But I think the best way to really learn a new programming language is actually use it for something.
So I'm contributing with a couple of opensource projects using Elixir: [riak-elixir-client][riak-client] and [mongoex][mongoex].


[jruby]: http://github.com/jruby/jruby
[cramp]: https://github.com/lifo/cramp
[elixir]: http://elixir-lang.org/
[fp]: http://en.wikipedia.org/wiki/Functional_programming
[elixir-book]: http://pragprog.com/book/elixir/programming-elixir
[riak-client]: https://github.com/drewkerrigan/riak-elixir-client
[mongoex]: https://github.com/mururu/mongoex
