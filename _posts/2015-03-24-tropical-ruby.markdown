---
layout: post
title:  "Tropical Ruby - Brazil"
date:   2015-03-24 11:00:00
---

Early this month, I had the opportunity to speak about [concurrent-ruby][concurrent-ruby] at [TropicalRuby Conference][tropicalrb] in Brazil.

<img src='/images/tropical-ruby1.jpg' style="width: 300px;"/>

It was an awesome event in an amazing [place][porto-de-galinhas].

<iframe src="//www.slideshare.net/slideshow/embed_code/46250859" width="476" height="400" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>


[tropicalrb]: http://tropicalrb.com/en/
[concurrent-ruby]: http://www.concurrent-ruby.com
[porto-de-galinhas]: https://en.wikipedia.org/wiki/Porto_de_Galinhas
