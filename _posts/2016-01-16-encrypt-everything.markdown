---
layout: post
title:  "Encrypt everything" 
date:   2016-01-16 11:00:00
---

<img src='/images/Encrypt_all_the_things.png'/>


This is a simple and short post, showing the tools I like to use.

### Computer

[HTTPS Everywhere][httpseverywhere] is a browser extension that encrypts your communications with many major websites, making your browsing more secure. 

[DNSEncrypt][dnsencrypt] turns regular DNS traffic into encrypted DNS traffic that is secure from eavesdropping and man-in-the-middle attacks.

[Off-the-Record Messaging][otr] allows you to have private conversations over instant messaging.

Use [OpenPGP][openpgp] to encrypt your emails or use [Protonmail][protonmail].

In case my computer is phisically compromised, I have [full disk encryption][diskencryption] enabled - so malicious users will not be able to read my files unless they have my secret key. 

I always try to avoid to ship my personal files to the cloud, I rather use [local backup with encryption enabled][readynas] instead.

### Mobile Device

For mobile device, I use Android with [CyanogenMod][cyanogenmod] and [Privacy Guard][privacyguard] enabled. I also replaced my SMS app with [Signal Private Messenger][whisper] - which allows me private messaging and private calling.

### Search 

[DuckDuckGo][duckduckgo] - Excellent search engine. It doesn't track you or store information about your searches. 

### Recommendations

I highly recommend you to take a look in [Encrypt All the things][encryptallthethings]. They have an excellent [guide][encryptallthethingspdf] about the importance of encryption.

[encryptallthethingspdf]: https://encryptallthethings.net/docs/EATT.pdf 
[protonmail]: https://www.protonmail.com
[encryptallthethings]: https://encryptallthethings.net/
[readynas]: http://www.amazon.com/NETGEAR-ReadyNAS-Attached-Diskless-RN10400-100NAS/dp/B00BNI4CVG
[diskencryption]: https://en.wikipedia.org/wiki/Disk_encryption
[httpseverywhere]: https://www.eff.org/https-everywhere
[otr]: https://en.wikipedia.org/wiki/Off-the-Record_Messaging
[dnsencrypt]: https://www.opendns.com/about/innovations/dnscrypt/
[openpgp]: http://www.pcworld.com/article/2472771/how-to-use-openpgp-to-encrypt-your-email-messages-and-files-in-the-cloud.html
[cyanogenmod]: http://www.cyanogenmod.org/
[whisper]: https://whispersystems.org/
[privacyguard]: https://en.wikipedia.org/wiki/Android_Privacy_Guard
[duckduckgo]: https://duckduckgo.com/
