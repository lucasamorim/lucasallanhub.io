---
layout: post 
title: "About"
description: ""
---

### {{ site.name }}

Hi, my name is Lucas Amorim and I'm a backend software developer in Vancouver, Canada.

My interest in computers started in 2003, when I was 15 years old and I found [Linux][conectiva] for the first time.

I started my Bachelor's Degree in Computer Science in 2007.  That same year I also began my first job in IT as a sysadmin and released my first open source tool: [gtool][gtool]. 

In 2008 I started my first job as a software developer.


### Open Source Projects

| Project   |      Description      |  Role |
|:----------|:-------------:|------:|
| [JRuby][jruby] |  Ruby on  JVM | core contributor |
| [concurrent-ruby][concurrent] | 	concurrency tools |   core contributor |
| [LuaTruffle][luatruffle] | Lua on JVM |    core contributor |
| [Gtool][gtool] | package management for Slackware |    core contributor |

### [PGP Public Key][mypgp]

{% highlight bash %}
➜  gpg --fingerprint 2528BB78 

pub   4096R/2528BB78 2016-01-14
      Key fingerprint = 1CDD 5F66 29BD 5DA6 0E5E  B55B 3590 028B 2528 BB78
uid                  Lucas Amorim <lucasamorim@protonmail.com>
sub   4096R/AFB42B35 2016-01-14

{% endhighlight %}

[mypgp]: http://pgp.mit.edu/pks/lookup?op=vindex&search=0x3590028B2528BB78
[conectiva]: https://en.wikipedia.org/wiki/Conectiva
[gtool]: http://sourceforge.net/projects/gtool/files/gtool/ 
[lua]: http://www.lua.org/
[luatruffle]: http://www.luatruffle.org/
[concurrent]: https://github.com/ruby-concurrency/concurrent-ruby
[jruby]: http://www.jruby.org
