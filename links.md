---
layout: post 
title: "Links"
description: ""
---

- [Yan][yan] - Excellent blog about cryptography, privacy and information freedom.
- [Sakurity][sakurity] - Web Security blog.
- [GrooveStomp][aaron] - One of the smartest guys I had a chance to work with and also a good friend, we share many interests.
- [Julia Evans][evans] - One of my favorites tech blog.

[aaron]: https://www.groovestomp.com/
[yan]: https://zyan.scripts.mit.edu/blog/
[sakurity]: http://sakurity.com/blog
[evans]: http://jvns.ca/
